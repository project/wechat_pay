<?php

namespace Drupal\wechat_pay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_price\Price;
use Drupal\commerce_refund\Entity\Refund;
use Drupal\commerce_refund\Entity\RefundInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\wechat_connect\Entity\WechatUser;
use Drupal\wechat_pay\WechatPayGatewayInterface;
use Symfony\Component\HttpFoundation\Request;
use EasyWeChat\Factory;
use EasyWeChat\Payment\Order;

/**
 * 微信支付
 *
 * @CommercePaymentGateway(
 *   id = "wechat_pay",
 *   label = @Translation("Wechat Pay"),
 *   display_label = @Translation("Wechat Pay"),
 *   modes = {
 *     "test" = @Translation("Testing"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\wechat_pay\PluginForm\PaymentForm",
 *   }
 * )
 */
class WechatPay extends OffsitePaymentGatewayBase implements SupportsRefundsInterface, WechatPayGatewayInterface {

  use StringTranslationTrait;

  /** @var  \EasyWeChat\Payment\Application $gateway_lib */
  protected $gateway_lib;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['client_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('调用此支付的微信应用类型'),
      '#options' => [
        self::CLIENT_TYPE_WEBSITE => 'Web网站应用',
        self::CLIENT_TYPE_H5 => 'H5手机网站应用',
        self::CLIENT_TYPE_MEDIA_PLATFORM => '微信公众号网页',
        self::CLIENT_TYPE_NATIVE_APP => '原生移动应用',
        self::CLIENT_TYPE_WEAPP => '小程序'
      ],
      '#default_value' => isset($this->configuration['client_type']) ? $this->configuration['client_type'] : self::CLIENT_TYPE_WEBSITE,
      '#required' => TRUE
    ];

    $form['appid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#description' => $this->t('绑定支付的APPID（开户邮件中可查看）'),
      '#default_value' => isset($this->configuration['appid']) ? $this->configuration['appid'] : '',
      '#required' => TRUE,
    ];

    $form['mch_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('商户号'),
      '#description' => $this->t('开户邮件中可查看'),
      '#default_value' => isset($this->configuration['mch_id']) ? $this->configuration['mch_id'] : '',
      '#required' => TRUE,
    ];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('商户支付密钥'),
      '#description' => $this->t('参考开户邮件设置（必须配置，登录商户平台自行设置）, 设置地址：https://pay.weixin.qq.com/index.php/account/api_cert'),
      '#default_value' => isset($this->configuration['key']) ? $this->configuration['key'] : '',
      '#required' => TRUE,
    ];

    $form['cert_pem_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cert证书路径'),
      '#description' => $this->t('apiclient_cert.pem'),
      '#default_value' => isset($this->configuration['cert_pem_path']) ? $this->configuration['cert_pem_path'] : ''
    ];

    $form['key_pem_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key证书路径'),
      '#description' => $this->t('apiclient_key.pem'),
      '#default_value' => isset($this->configuration['key_pem_path']) ? $this->configuration['key_pem_path'] : ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['client_type'] = $values['client_type'];
      $this->configuration['appid'] = $values['appid'];
      $this->configuration['mch_id'] = $values['mch_id'];
      $this->configuration['key'] = $values['key'];
      $this->configuration['cert_pem_path'] = $values['cert_pem_path'];
      $this->configuration['key_pem_path'] = $values['key_pem_path'];
    }
  }

  /**
   * {@inheritdoc}
   * @param PaymentInterface $payment
   * @param Price|null $amount
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    // Validate the requested amount.
    $this->assertRefundAmount($payment, $amount);

    if (!$this->gateway_lib) {
      $this->loadGatewayConfig();
    }

    $config = $this->gateway_lib->config;

    if (!$this->gateway_lib->config['cert_path'] || !$this->gateway_lib->config['key_path']) {
      throw new \InvalidArgumentException($this->t('Could not load the apiclient_cert.pem or apiclient_key.pem files, which are required for WeChat Refund. Did you configure them?'));
    }

    // 创建一个退款单
    $refund = Refund::create([
      'payment_id' => $payment->id(),
      'amount' => $amount,
      'state' => 'new',
      'remark' => '订单退款'
    ]);
    $refund->save();

    $result = $this->gateway_lib->refund->byTransactionId($payment->getRemoteId(),
      $refund->id(),
      floatval($payment->getAmount()->getNumber()) * 100,
      floatval($amount->getNumber()) * 100,
      ['refund_desc' => '订单['.$payment->getOrder()->getOrderNumber().']退款']);

    if ($result['return_code'] !== 'SUCCESS' || $result['result_code'] !== 'SUCCESS') {
      // For any reason, we cannot get a preorder made by WeChat service
      throw new InvalidRequestException($this->t('调用微信退款接口失败: ') . $result['err_code_des']);
    } else {

      $refund->setRemoteId($result['refund_id']);
      $refund->setRemoteState('pending');
      $refund->save();

      $old_refunded_amount = $payment->getRefundedAmount();
      $new_refunded_amount = $old_refunded_amount->add($amount);
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      } else {
        $payment->setState('refunded');
      }

      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();

      \Drupal::messenger()->addMessage('通退请求已成功发送到微信服务器！');
    }
  }

  /**
   * {@inheritdoc}
   * @throws \EasyWeChat\Kernel\Exceptions\Exception
   */
  public function onNotify(Request $request) {

    if (!$this->gateway_lib) {
      $this->loadGatewayConfig();
    }

    if (isset($result['refund_id'])) { // TODO:: 从请求判断 $request
      // 退款通知
      $response = $this->gateway_lib->handleRefundedNotify(function ($message, $reqInfo, $fail) {

        $result = $message;
        if ($result['return_code'] !== 'SUCCESS' || $result['refund_status'] !== 'SUCCESS') {
          $refund = Refund::load($result['refund_id']);
          if ($refund instanceof RefundInterface) {
            $refund->setRemoteState($result['refund_status']);
            $refund->setCompletedTime(time());
            $refund->setState('completed');
            $refund->save();

            // 退款完成事件

            return true;
          }
        }

        $fail('退款通知通信没有成功');
      });

      return $response;
    } else {
      // 支付结果通知

      $response = $this->gateway_lib->handlePaidNotify(function ($message, $fail) {
        $result = $message;

        // 记录系统日志
        if ($this->getMode()) {
          \Drupal::logger('wechat_pay')->notice('接收到来自微信支付的通知：' . print_r($result, TRUE));
        }

        if ($result['return_code'] === 'SUCCESS' && $result['result_code'] === 'SUCCESS' ) {

          // load the payment
          $payment_id = str_replace('payment', '', explode('-', $result['out_trade_no'])[0]);
          $payment = Payment::load($payment_id);
          if ($payment instanceof PaymentInterface) {

            $order = $payment->getOrder();
            if ($order instanceof OrderInterface) {

              $payment->setState('completed');
              $payment->setRemoteId($result['transaction_id']);
              $payment->save();

              // 因为现在订单的place转换是commerce 自行管理的，所以不再需要手动转换
              // Payment Entity 会在支付完成时，调用 commerce_payment.order_updater 服务去更新
              // Order Entity 的已支付金额，如果已支付金额达到订单的需支付金额，订单会自动 place
              // =========================================================================
              // $transition = $order->getState()->getWorkflow()->getTransition('place');
              // $order->getState()->applyTransition($transition);
              // $order->save();

              return true;

            } else {
              $msg = '找不到支付单[' . $payment->id() . ']所关联的订单';
              \Drupal::logger('wechat_pay')->error($msg. print_r($result, TRUE));
              $fail($msg);
            }

          } else {
            $msg = 'out_trade_no[' . $result['out_trade_no'] . ']不是有效的支付单';
            \Drupal::logger('wechat_pay')->error( $msg. print_r($result, TRUE));
            $fail($msg);
          }

        } else { // When payment failed or communication failed
          $msg = '支付没有成功';
          \Drupal::logger('wechat_pay')->error( $msg . print_r($result, TRUE));
          $fail($msg);
        }
      });

      return $response;
    }
  }

  /**
   * @param \Drupal\commerce_order\Entity\Order $commerce_order
   * @return Payment
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createPayment(\Drupal\commerce_order\Entity\Order $commerce_order) {
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $commerce_order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $commerce_order,
      'test' => $this->getMode() === 'test'
    ]);

    $payment->save();

    return $payment;
  }

  /**
   * Load configuration from parameters first, otherwise from system configuration. This method exists so other part of system can override the configurations.
   * One use case would be multi-stores, each store has its own payment gateway configuration saved on other entity.
   * @param null $appid
   * @param null $mch_id
   * @param null $key
   * @param null $cert_path
   * @param null $key_path
   * @param null $mode
   * @param null $sub_appid
   * @param null $sub_mch_id
   */
  public function loadGatewayConfig($appid = NULL, $mch_id = NULL, $key = NULL, $cert_path = NULL, $key_path = NULL, $mode = NULL, $sub_appid = NULL, $sub_mch_id = NULL) {
    if (!$appid) {
      $appid = $this->getConfiguration()['appid'];
    }
    if (!$mch_id) {
      $mch_id = $this->getConfiguration()['mch_id'];
    }
    if (!$key) {
      $key = $this->getConfiguration()['key'];
    }
    if (!$cert_path) {
      $cert_path = $this->getConfiguration()['cert_pem_path'];
    }
    if (!$key_path) {
      $key_path = $this->getConfiguration()['key_pem_path'];
    }
    if (!$mode) {
      $mode = $this->getMode();
    }

    $options = [
      'app_id' => $appid,
      'mch_id' => $mch_id,
      'key' => $key,
      'cert_path' => $cert_path,
      'key_path' => $key_path
    ];

    if ($mode === 'test') {
      // $options['sandbox'] = true;
      // 暂时不使用沙箱调试，因为下单时会出现： 沙箱支付金额(1)无效，请检查需要验收的case
    }

    $app = Factory::payment($options);
    $this->gateway_lib = $app;
  }

  private function getRemoteId($result) {
    $remote_id = NULL; // There is no $remote_id when USERPAYING

    if (array_key_exists('transaction_id', $result)) {
      $remote_id = $result['transaction_id'];
    } elseif (array_key_exists('prepay_id', $result)) {
      $remote_id = $result['prepay_id'];
    }

    return $remote_id;
  }

  /**
   *
   * @param $commerce_order
   * @return void
   * @throws \Exception
   */
  public function requestQRCode($commerce_order) {
    if (!$this->gateway_lib) {
      $this->loadGatewayConfig();
    }
    $client_type = $this->getConfiguration()['client_type'];
    if ($client_type !== self::CLIENT_TYPE_WEBSITE) {
      throw new \Exception('requestQRCode only support [' . self::CLIENT_TYPE_WEBSITE . '] client type.');
    }
    $code_url = $this->createUnifiedOrder($commerce_order, self::UNIFIED_ORDER_TRADE_TYPE_NATIVE)['code_url'];

    return $code_url;
  }

  /**
   * @param $commerce_order
   * @return null
   * @throws \Exception
   */
  public function getClientLaunchConfig(OrderInterface $commerce_order) {
    $config = null;
    $client_type = $this->getConfiguration()['client_type'];
    switch ($client_type) {
      case self::CLIENT_TYPE_MEDIA_PLATFORM:
        $prepayId = $this->createUnifiedOrder($commerce_order, self::UNIFIED_ORDER_TRADE_TYPE_JSAPI)['prepay_id'];
        $config = $this->gateway_lib->jssdk->sdkConfig($prepayId);
        break;

      case self::CLIENT_TYPE_NATIVE_APP:
        $prepayId = $this->createUnifiedOrder($commerce_order, self::UNIFIED_ORDER_TRADE_TYPE_APP)['prepay_id'];
        $config = $this->gateway_lib->jssdk->appConfig($prepayId);
        break;

      case self::CLIENT_TYPE_WEAPP:
        $prepayId = $this->createUnifiedOrder($commerce_order, self::UNIFIED_ORDER_TRADE_TYPE_JSAPI)['prepay_id'];
        $config = $this->gateway_lib->jssdk->bridgeConfig($prepayId, false);
        break;

      default:
        throw new \Exception('未知或未实现的 Client Type');
    }

    if (!isset($config['timeStamp']) && isset($config['timestamp'])) $config['timeStamp'] = $config['timestamp'];


    return $config;
  }

  public function createUnifiedOrder($commerce_order, $trade_type) {
    /** @var \Drupal\commerce_order\Entity\Order $commerce_order */

    // 调用 overtrue SDK，统一下单
    if (!$this->gateway_lib) {
      $this->loadGatewayConfig();
    }

    $order_item_names = '';
    foreach ($commerce_order->getItems() as $order_item) {
      /** @var OrderItem $order_item */
      $order_item_names .= $order_item->getTitle() . ', ';
    }


    global $base_url;
    $notify_url = $base_url . '/' . $this->getNotifyUrl()->getInternalPath();

    $payment = $this->createPayment($commerce_order);

    $total_fee = $commerce_order->getTotalPrice()->getNumber() * 100;
    if ($this->getMode() === 'test') $total_fee = 600;

    $attributes = [
      'trade_type' => $trade_type, // JSAPI，NATIVE，APP...
      'body' => \Drupal::config('system.site')->get('name') . $this->t(' Order: ') . $commerce_order->getOrderNumber(),
      'detail' => $order_item_names,
      'out_trade_no' => 'payment'.$payment->id(). '-' . time(),
      'total_fee' => $total_fee, // 单位：分
      'notify_url' => $notify_url // 支付结果通知网址，如果不设置则会使用配置里的默认地址
    ];

    if ($trade_type === self::UNIFIED_ORDER_TRADE_TYPE_JSAPI) {
      // 查看用户 open_id
      /** @var \Drupal\Core\Entity\Query\QueryInterface $query */
      $query = \Drupal::entityQuery('wechat_user')
        ->condition('user_id', \Drupal::currentUser()->id())
        ->condition('app_id', $this->getConfiguration()['appid']);
      $ids = $query->execute();

      if (empty($ids)) throw new \Exception('找不到当前用户 ' . \Drupal::currentUser()->getAccountName() . ':' . \Drupal::currentUser()->id() . ' 的 openid，');

      /** @var WechatUser $wechat_user */
      $wechat_user = WechatUser::load(array_pop($ids));
      $attributes['openid'] = $wechat_user->getOpenId(); // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识，
    }

    $result = $this->gateway_lib->order->unify($attributes);
    if ($result['return_code'] == 'SUCCESS') {
      $prepayId = $result['prepay_id'];

      // 更新Payment
      $payment->setState('authorization');
      $payment->setAuthorizedTime(time());
      $payment->setRemoteId($this->getRemoteId($result));
      $payment->setRemoteState($result['result_code']);
      $payment->save();

      return [
        'prepay_id' => $prepayId,
        'payment' => $payment,
        'code_url' => $trade_type === self::UNIFIED_ORDER_TRADE_TYPE_NATIVE ? $result['code_url'] : null
      ];
    } else {
      throw new \Exception('下单错误'.var_export($result, true).var_export($attributes, true));
    }
  }
}
