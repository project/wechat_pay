<?php

namespace Drupal\wechat_pay;

use Drupal\commerce_checkout_api\SupportHeadlessPaymentInterface;

interface WechatPayGatewayInterface extends SupportHeadlessPaymentInterface {

  const CLIENT_TYPE_WEBSITE = 'website';
  const CLIENT_TYPE_MEDIA_PLATFORM = 'media_platform';
  const CLIENT_TYPE_NATIVE_APP = 'native_app';
  const CLIENT_TYPE_H5 = 'h5';
  const CLIENT_TYPE_WEAPP = 'weapp';

  const UNIFIED_ORDER_TRADE_TYPE_JSAPI = 'JSAPI';
  const UNIFIED_ORDER_TRADE_TYPE_APP = 'APP';
  const UNIFIED_ORDER_TRADE_TYPE_NATIVE = 'NATIVE';

  public function createUnifiedOrder($commerce_order, $trade_type);
}
