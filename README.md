# Wechat Pay

Provides payment gateway for `commerce_payment` with wechat pay.

## Features

- 支持手机APP支付
- 支付PC网站描码支付（未完成）
- 支持小程序支付（未完成）
- 支付退款

## dependencies

- `drupal/commerce_refund` 实现退款状态跟踪
